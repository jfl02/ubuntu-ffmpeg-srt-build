# ubuntu-ffmpeg-srt-build

Script to build an srt enabled ffmpeg on Ubuntu.

# usage

*warning, install.sh is destructive. It removes the previous build.*

`> source install.sh`

It mainly followed the instuctions from here: https://trac.ffmpeg.org/wiki/CompilationGuide/Ubuntu

It also add the SRT dependency: https://github.com/Haivision/srt

Sources directory is $HOME/ffmpeg_sources/

Build prefix is: $HOME/ffmpeg_build

Resulting applications are copied to $HOME/bin

